import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './views/Dashboard.vue'
import Properties from './views/Properties.vue'
import UserManagement from './views/UserManagement.vue'



Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/properties',
      name: 'properties',
      component: Properties
    },
    {
      path: '/usermanagement',
      name: 'usermanagement',
      component: UserManagement
    },
  ]
})
